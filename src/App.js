import React, { Component } from 'react';
import Header from './componentes/Header';
import Noticias from './componentes/Noticias';
import Formulario from './componentes/Formulario';

class App extends Component {

  state = {
    noticias : []
  }

  componentDidMount () {
    this.consultarNoticias();
  }

  consultarNoticias = (categoria ='general') => {
    // console.log(categoria);
    let url = `https://newsapi.org/v2/top-headlines?country=us&category=${categoria}&apiKey=5013fe2fa43c4ef580cfc3f59684c041`;

    fetch(url)
      .then(respuesta => {
        return respuesta.json();
      })
      .then(noticias => {
        this.setState ({
          noticias : noticias.articles
        })
      })
  }

  render() {
    return (
      <div className="contenerdor-app">
        <Header 
          titulo = "Noticias"
        />


        <div className='container white contenedor-noticias'>
        <Formulario
          consultarNoticias = {this.consultarNoticias}
        />
          <Noticias 
            noticias = {this.state.noticias}
          />
        </div>
      </div>
    );
  }
}

export default App;
